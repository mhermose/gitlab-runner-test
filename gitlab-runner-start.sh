#!/bin/bash
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

GITLAB_CONFIG_FILE=/dsc/local/etc/gitlab-runner-config.toml
BIN_FOLDER=/nfs/cwe-513-vpl401/opt/mhermose/workspaces/mockturtle/gitlab-runner/bin

if [ ! -f $GITLAB_CONFIG_FILE ] ; then
    echo "Gitlab runner not registered. Please run $BIN_FOLDER/register-runner.sh" >&2
    exit 1
fi

# This is only needed if we want to execute the runner as user 'gitlab-runner'
# However, in our case we have to run as root so that we can access the drivers
#
# if ! id gitlab-runner ; then
#     echo "Adding gitlab-runner user"
#     useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
# fi
#
# We need to modify the PATH so that gitlab-runner will find the `git` command, which is not installed on FECs
# sudo -i --user=gitlab-runner sh -c \
#     "export PATH=\"\$PATH:$BIN_FOLDER\" ; gitlab-runner run --config \"$GITLAB_CONFIG_FILE\""
#

export PATH=$PATH:$BIN_FOLDER
gitlab-runner run --config "$GITLAB_CONFIG_FILE"
