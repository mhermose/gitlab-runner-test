# Gitlab Runner of FECs

To add a FEC runner to your Gitlab Project:

Gitlab
- In your project go to "Settings" > "CI/CD"
- Under "Runners", click "Expand"
- Copy the registration token shown there

FEC
- Ssh to your FEC
- Navigate to this repo
- Run `gitlab-runner-register.sh <MY_GITLAB_TOKEN>`

CCDE
- Go to your FEC config, and under "Logical configuration" add the following program:
    - Porgram: `LUMENS_EXEC_SIMPLE`
    - Parameter `$1`: Location of this repo (eg: `/nfs/cwe-513-vpl401/opt/mhermose/gitlab-test/gitlab-runner`)
    - Parameter `$2`: `gitlab-runner-start.sh`
- Rebuild the FEC with `lumensctl -H <FECNAME> regenerate`


