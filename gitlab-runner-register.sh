#!/bin/bash
set -o errexit   # abort on nonzero exitstatus
set -o nounset   # abort on unbound variable
set -o pipefail  # don't hide errors within pipes

BASE_PATH=$(dirname "$(realpath "$0")")
GITLAB_CONFIG_FILE=/dsc/local/etc/gitlab-runner-config.toml

usage()
(
    cat << EOF >&2
Registers a gitlab runner with the provided registration token

Usage: $0 TOKEN

  TOKEN:
      Gitlab registration token. You can find the registration token
      for your gitlab project under:'settings' > 'CI/CD' > 'Runners'

  -h, --help:
      display this help and exit

EOF
)

if [ "$#" -ne 1 ] ; then
    usage
    exit 1
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then
    usage
    exit 0
fi

"$BASE_PATH"/bin/gitlab-runner register \
    --non-interactive \
    --url=http://gitlab.cern.ch \
    --executor "shell" \
    --name "$(hostname)-runner" \
    --tag-list "fec-runner" \
    --run-untagged="false" \
    --locked="true" \
    --registration-token "$1"

if [ -f "$HOME/.gitlab-runner/config.toml" ]; then
    cp "$HOME/.gitlab-runner/config.toml" $GITLAB_CONFIG_FILE
    chmod +r $GITLAB_CONFIG_FILE
    echo "Configuration file succesfully copied to $GITLAB_CONFIG_FILE"
else
    echo "Please, copy the generated 'config.toml' file to $GITLAB_CONFIG_FILE"
fi
